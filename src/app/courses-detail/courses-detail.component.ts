import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-courses-detail',
  templateUrl: './courses-detail.component.html',
  styleUrls: ['./courses-detail.component.css']
})
export class CoursesDetailComponent {
  course: string = '';

  // Para recuperar el parámetro de la url "/courses/:course" 
  // haremos lo siguiente:
  // Importamos la clase "ActivatedRoute" para recibir los 
  // parámetros que nos van a llegar por la url.
  // Inyectamos la clase "ActivatedRoute". Definimos la propiedad 
  // privada para poder utilizarla sólo dentro de nuestra clase.
  constructor(private route: ActivatedRoute) {
    // Recibimos un objeto con los parámetros.
    // this.route.params.subscribe(params => console.log(params));
    // Recuperams el parámetro con la variable llamada course.
    // Trasladamos éste código al método ngOnInit para dejar más 
    // limpio el constructor.
    // this.route.params.subscribe(params => {
    //   this.course = params['course']
    // });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.course = params['course']
    });
  }

  // Hemos visto cómo pasar parámentros por la url, éstos 
  // parámetros luego los podemos utilizar para hacer consultas 
  // a la bases de datos y traer la información que necesitamos 
  // pintar.
}
