import { Component } from '@angular/core';

@Component({
  selector: 'app-formulario-plantillas',
  templateUrl: './formulario-plantillas.component.html',
  styleUrls: ['./formulario-plantillas.component.css']
})
export class FormularioPlantillasComponent {
  persona = {
    nombre: '',
    edad: ''
  };

  procesar(): void {
    console.log(this.persona);
  }
}
