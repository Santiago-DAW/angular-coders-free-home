import { Component } from '@angular/core';
import { Persona } from '../models/Persona.model';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.css']
})
export class ContadorComponent {
  numero: number = 1;
  numero_2: number = 1;

  incrementar() {
    this.numero_2++;
    // this.numero_2 += 1;
  }

  decrementar() {
    // this.numero_2--;
    this.numero_2 -= 1;
  }
}
