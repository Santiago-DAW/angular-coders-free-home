import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListCoursesComponent } from './pages/list-courses/list-courses.component';

const routes: Routes = [
  {
    // path: ''     // path: 'courses' // Hereada el path del padre.
    // path: 'prueba'  // path: 'courses/prueba'
    path: '',
    component: ListCoursesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Courses2RoutingModule { }
