import { Component } from '@angular/core';

@Component({
  selector: 'app-botones',
  templateUrl: './botones.component.html',
  styleUrls: ['./botones.component.css']
})
export class BotonesComponent {
  // text_color: string = '';
  // button_disabled: boolean = false;

  src: string = 'https://images.pexels.com/photos/9954174/pexels-photo-9954174.jpeg?auto=compress&cs=tinysrgb&w=300&lazy=load';
  src_1: string = 'https://images.pexels.com/photos/17041987/pexels-photo-17041987/free-photo-of-mujer-andina-en-la-plaza-mayor-de-cusco.jpeg?auto=compress&cs=tinysrgb&w=300&lazy=load';
  src_2: string = 'https://images.pexels.com/photos/17115609/pexels-photo-17115609/free-photo-of-madera-edificio-pared-casa.jpeg?auto=compress&cs=tinysrgb&w=300&lazy=load';
}
