import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.css']
})
export class ChildrenComponent {
  @Input() title?: string;

  // Creamos una instancia de la clase EventEmitter.
  // Éste evento todavía no se puede transmitir al componente 
  // padre.
  //    titleChange = new EventEmitter<string>();
  // Usamos @Output() para especificar que queremos que éste 
  // evento se transmita hacia el componente padre.
  @Output() titleChange = new EventEmitter<string>();

  emitTitleChage() {
    // console.log(this.title);
    // Le pasamos como valor opcional el Input() title para 
    // avisarle al componente padre por que valor ha cambiado.
    this.titleChange.emit(this.title);
  }
}
