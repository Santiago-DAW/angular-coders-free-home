import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Blog2RoutingModule } from './blog2-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    Blog2RoutingModule
  ]
})
export class Blog2Module { }
