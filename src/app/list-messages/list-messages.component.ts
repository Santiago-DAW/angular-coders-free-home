import { Component } from '@angular/core';
import { MessagesService } from '../services/messages.service';

@Component({
  selector: 'app-list-messages',
  templateUrl: './list-messages.component.html',
  styleUrls: ['./list-messages.component.css']
})
export class ListMessagesComponent {
  // Inyectamos el servicio en una variable llamada messageService.
  // Lo definimos public para acceder desde ésta clase (ts) como 
  // al archivo html de éste componente.
  constructor(public messageService: MessagesService) { }
}
