import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  // Almacenamos toda la información.
  messages: string[] = ['a', 'b'];

  constructor() { }

  // Los servicios tanbién deben tener todos los métodos necesarios 
  // para poder recuperar esa información, para poder editarla, 
  // para poder eliminarla, etc.
  add(message: string): void {
    this.messages.push(message);
  }
}
