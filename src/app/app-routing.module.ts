import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { 
    path: '',
    loadChildren: () => import('./welcome/welcome.module')
      .then(m => m.WelcomeModule)
  },
  {
    path: 'courses',
    loadChildren: () => import('./courses2/courses2.module')
      .then(m => m.Courses2Module)
  },
  {
    path: 'blog',
    loadChildren: () => import('./blog2/blog2.module')
      .then(m => m.Blog2Module)
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
