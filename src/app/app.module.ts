import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ContadorComponent } from './contador/contador.component';
import { BotonesComponent } from './botones/botones.component';
import { FormularioComponent } from './formulario/formulario.component';
import { BuclesComponent } from './bucles/bucles.component';
import { SwitchComponent } from './switch/switch.component';
import { FormularioPlantillasComponent } from './formulario-plantillas/formulario-plantillas.component';
import { FormularioReactivoComponent } from './formulario-reactivo/formulario-reactivo.component';
import { ChildrenComponent } from './children/children.component';
import { AddMessagesComponent } from './add-messages/add-messages.component';
import { ListMessagesComponent } from './list-messages/list-messages.component';

import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CoursesComponent } from './courses/courses.component';
import { CoursesDetailComponent } from './courses-detail/courses-detail.component';
import { ContactInfoComponent } from './contact-info/contact-info.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';

import { AppRoutingModule } from './app-routing.module';
// import { Home2Component } from './welcome/home2/home2.component';

// El módulo principal se tiene que enterar de la existencia del 
// módulo appRouting. Para poder utilizar el módulo de rutas 
// appRouting lo tenemos que incluir en el módulo principal, para 
// ello importamos AppRoutingModule y lo incluimos en imports.

@NgModule({
  declarations: [
    AppComponent,
    ContadorComponent,
    BotonesComponent,
    FormularioComponent,
    BuclesComponent,
    SwitchComponent,
    FormularioPlantillasComponent,
    FormularioReactivoComponent,
    ChildrenComponent,
    AddMessagesComponent,
    ListMessagesComponent,
    // Componentes enrutamiento:
    HomeComponent,
    ContactComponent,
    AboutComponent,
    PageNotFoundComponent,
    CoursesComponent,
    CoursesDetailComponent,
    ContactDetailsComponent,
    // Home2Component,
  ],
  imports: [
    BrowserModule,
    // Inclimos el módulo FormsModule para poder utilizar ngModel.
    // formularios de plantilla.
    FormsModule,
    // Incluimos el módulo ReactiveFormsModule para trabajar con 
    // formularios reactivos.
    ReactiveFormsModule,
    // Incluimos nuestro sistema de rutas.
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
