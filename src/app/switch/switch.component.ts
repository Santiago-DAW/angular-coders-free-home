import { Component } from '@angular/core';

@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.css']
})
export class SwitchComponent {
  // dia: number = 1;   // Lunes (em el html);
  // dia: number = 5;   // Viernes (em el html);
  // dia: number = 9;   // Valor no válido (em el html);
  
  // dia: string = '1';

  // Si no queremos asignarle ningún valor por defecto, en éste 
  // caso, podemos hacer lo siguiente:
  //    dia?: string;
  // Cuando defino el símbolo de interrogación, le estoy diciendo 
  // a Angular que quiero definir ésta propiedad pero inicialmente 
  // no le quiero dar ningún valor.
  dia?: string;
}
