import { Component } from '@angular/core';
import { MessagesService } from '../services/messages.service';

@Component({
  selector: 'app-add-messages',
  templateUrl: './add-messages.component.html',
  styleUrls: ['./add-messages.component.css']
})
export class AddMessagesComponent {
  message: string = '';

  // Inyectamos el servicio en una variable llamada messageService.
  // Lo definimos public para acceder desde ésta clase (ts) como 
  // al archivo html de éste componente.
  constructor(public messagesService: MessagesService) { }

  addMessage() {
    if (this.message)
      this.messagesService.add(this.message);
    this.message = '';
  }
}
